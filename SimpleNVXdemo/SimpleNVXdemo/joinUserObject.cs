﻿using System;
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharpPro;
using Crestron.SimplSharpPro.DeviceSupport;

namespace SimpleNVXdemo
{
        public class DigitalJoinUserObject
        {
            public Action<BasicTriList> DigitalPressAction { get; set; }
            public Action<BasicTriList> DigitalReleaseAction { get; set; }

            public static void emptymethod(BasicTriList tp)
            {
            }
        }
        public class AnalogJoinUserObject
        {
            public Action<BasicTriList, SigEventArgs> AnalogSetAction { get; set; }
        }
        public class SerialJoinUserObject
        {
            public Action<BasicTriList, SigEventArgs> SerialSetAction { get; set; }
        }
    }

