﻿using System;
using Crestron.SimplSharpPro.DM.Streaming;
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharpPro;
using Crestron.SimplSharpPro.DeviceSupport;
using Crestron.SimplSharpPro.DM;

namespace SimpleNVXdemo
{
    public class Class1
    {
        public static DmNvx350 transmitter;
        public static DmNvx350 receiver;
        public static Tsw750 tp;

        public static void createHardware(CrestronControlSystem processor)
        {
            tp = new Tsw750(0x3, processor);
            tp.Register();
            transmitter = new DmNvx350(0x4, processor);
            transmitter.Register();
            receiver = new DmNvx350(0x5, processor);
            receiver.Register();

            tp.SigChange += Touchpanel_SigChange;

            transmitter.EndpointName = "transmitter";
            receiver.EndpointName = "receiver";
            transmitter.XioRouting.XioRoutingChange += XioRouting_XioRoutingChange1;
        }

        private static void XioRouting_XioRoutingChange1(object sender, GenericEventArgs args)
        {
            switch (args.EventId)
            {
                case (DMInputEventIds.ActiveVideoSourceEventId):
                    {
                        //nvx.XioRouting.SubscribeStreams.DiscoveredDevicesFeedback.UShortValue;
                        //args.i
                        break;
                    }
            }
            }

        private static void Touchpanel_SigChange(BasicTriList currentDevice, SigEventArgs args)
        {
            switch (args.Sig.Type)
            {
                case eSigType.Bool:
                    {
                        if (args.Sig.BoolValue)
                        {
                            (args.Sig.UserObject as DigitalJoinUserObject).DigitalPressAction(currentDevice);
                            break;
                        }
                        else
                        {
                            (args.Sig.UserObject as DigitalJoinUserObject).DigitalReleaseAction(currentDevice);
                            break;
                        }
                    }

                case eSigType.UShort:
                    {
                        (args.Sig.UserObject as AnalogJoinUserObject).AnalogSetAction(currentDevice, args);
                        break;
                    }

                case eSigType.String:
                    {
                        (args.Sig.UserObject as SerialJoinUserObject).SerialSetAction(currentDevice, args);
                        break;
                    }
            }
        }

        public static void AddActionstoTPJoins()
        {

            (tp.BooleanOutput[1].UserObject as DigitalJoinUserObject).DigitalPressAction = new Action<BasicTriList>((input) => route(1));
            (tp.BooleanOutput[2].UserObject as DigitalJoinUserObject).DigitalPressAction = new Action<BasicTriList>((touchpanel) => route(2));
            (tp.BooleanOutput[3].UserObject as DigitalJoinUserObject).DigitalPressAction = new Action<BasicTriList>((touchpanel) => route(3));
            (tp.BooleanOutput[4].UserObject as DigitalJoinUserObject).DigitalPressAction = new Action<BasicTriList>((touchpanel) => route(4));

        }

        public static void route(ushort input)
        {
            receiver.XioRouting.VideoOut.UShortValue = input;
        }



    }
}
