using System;
using System.IO;
using Crestron.SimplSharp;                              // For Basic SIMPL# Classes
using Crestron.SimplSharpPro;                           // For Basic SIMPL#Pro classes
using Crestron.SimplSharpPro.CrestronThread;            // For Threading
using Crestron.SimplSharpPro.Diagnostics;               // For System Monitor Access
using Crestron.SimplSharp.WebScripting;                 // For Programmatic REST API


namespace SimpleNVXdemo
{
    public class Server_Handler : IHttpCwsHandler
    {
        private ControlSystem _cs;
        public Server_Handler(ControlSystem cs)
        {
            _cs = cs;
        }

        void IHttpCwsHandler.ProcessRequest(HttpCwsContext context)
        {
            try
            {
                // handle requests
                if (context.Request.RouteData != null)
                {
                    switch (context.Request.RouteData.Route.Name.ToUpper())
                    {
                        case "ROOMNAME":
                            context.Response.StatusCode = 200;
                            context.Response.Write(InitialParametersClass.RoomName, true);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorLog.Error("CWS Handler: {0}", e.Message);
            }

        }
    }

    public class ControlSystem : CrestronControlSystem
    {

        private string myFolder { get { return Crestron.SimplSharp.CrestronIO.Directory.GetApplicationRootDirectory() + "/User/"; } }
        private HttpCwsServer myServer;

        public ControlSystem()
            : base()
        {
            try
            {
                Thread.MaxNumberOfUserThreads = 20;
                //Subscribe to the controller events (System, Program, and Ethernet)
                CrestronEnvironment.SystemEventHandler += new SystemEventHandler(ControlSystem_ControllerSystemEventHandler);
                CrestronEnvironment.ProgramStatusEventHandler += new ProgramStatusEventHandler(ControlSystem_ControllerProgramEventHandler);
                CrestronEnvironment.EthernetEventHandler += new EthernetEventHandler(ControlSystem_ControllerEthernetEventHandler);

                ErrorLog.Notice("*** : Constructor");
                ErrorLog.Notice("*** : Current Time is {0}", DateTime.Now);
                myServer = new HttpCwsServer("api");
                myServer.Routes.Add(new HttpCwsRoute("roomname") { Name = "ROOMNAME" });
                myServer.HttpRequestHandler = new Server_Handler(this);
                myServer.Register();

            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in the constructor: {0}", e.Message);
            }
        }

        public override void InitializeSystem()
        {
            try
            {
                ErrorLog.Notice("*** : InitializeSystem");
                using (FileStream myStream = File.Create(myFolder + "test.txt"))
                {
                    byte[] whatToWrite = System.Text.Encoding.ASCII.GetBytes(String.Format("Program Running Name: {0}", InitialParametersClass.RoomName));
                    myStream.Write(whatToWrite,0,whatToWrite.Length);
                    myStream.Flush();
                }

                Class1.createHardware(this);
                Class1.AddActionstoTPJoins();
            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in InitializeSystem: {0}", e.Message);
            }

        }

        void ControlSystem_ControllerEthernetEventHandler(EthernetEventArgs ethernetEventArgs)
        {
            switch (ethernetEventArgs.EthernetEventType)
            {//Determine the event type Link Up or Link Down
                case (eEthernetEventType.LinkDown):

                    if (ethernetEventArgs.EthernetAdapter == EthernetAdapterType.EthernetLANAdapter)
                    {
                        //
                    }
                    break;
                case (eEthernetEventType.LinkUp):
                    if (ethernetEventArgs.EthernetAdapter == EthernetAdapterType.EthernetLANAdapter)
                    {

                    }
                    break;
            }
        }

        /// <summary>
        /// Event Handler for Programmatic events: Stop, Pause, Resume.
        /// Use this event to clean up when a program is stopping, pausing, and resuming.
        /// This event only applies to this SIMPL#Pro program, it doesn't receive events
        /// for other programs stopping
        /// </summary>
        /// <param name="programStatusEventType"></param>
        void ControlSystem_ControllerProgramEventHandler(eProgramStatusEventType programStatusEventType)
        {
            switch (programStatusEventType)
            {
                case (eProgramStatusEventType.Paused):
                    //The program has been paused.  Pause all user threads/timers as needed.
                    ErrorLog.Notice("*** : Pause");
                    break;
                case (eProgramStatusEventType.Resumed):
                    //The program has been resumed. Resume all the user threads/timers as needed.
                    ErrorLog.Notice("*** : Resumed");
                    break;
                case (eProgramStatusEventType.Stopping):
                    //The program has been stopped.
                    //Close all threads.
                    //Shutdown all Client/Servers in the system.
                    //General cleanup.
                    //Unsubscribe to all System Monitor events
                    ErrorLog.Notice("*** : Stopping");
                    break;
            }

        }

        /// <summary>
        /// Event Handler for system events, Disk Inserted/Ejected, and Reboot
        /// Use this event to clean up when someone types in reboot, or when your SD /USB
        /// removable media is ejected / re-inserted.
        /// </summary>
        /// <param name="systemEventType"></param>
        void ControlSystem_ControllerSystemEventHandler(eSystemEventType systemEventType)
        {
            switch (systemEventType)
            {
                case (eSystemEventType.DiskInserted):
                    //Removable media was detected on the system
                    ErrorLog.Notice("*** : Disk Inserted");
                    break;
                case (eSystemEventType.DiskRemoved):
                    //Removable media was detached from the system
                    ErrorLog.Notice("*** : Disk Removed");
                    break;
                case (eSystemEventType.Rebooting):
                    //The system is rebooting.
                    //Very limited time to preform clean up and save any settings to disk.
                    ErrorLog.Notice("*** : Rebooting");
                    break;
            }

        }
    }
}
